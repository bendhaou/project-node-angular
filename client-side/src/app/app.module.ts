import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './authentification/login/login.component';
import { RegisterComponent } from './authentification/register/register.component';
import { ForgotpasswordComponent } from './authentification/forgotpassword/forgotpassword.component';
import { NewpasswordComponent } from './authentification/newpassword/newpassword.component';
import { AuthComponent } from './authentification/auth/auth.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './authentification/auth.service';

import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule }   from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ResetComponent } from './reset/reset.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent},
  { path: '', component: AuthComponent ,
  children: [
        { path: '', component: LoginComponent },
        { path: 'register', component: RegisterComponent },
        { path: 'forgotpassword', component: ForgotpasswordComponent },
        { path: 'newpassword/:token', component: NewpasswordComponent }

      ]

}

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    NewpasswordComponent,
    AuthComponent,
    HomeComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,FormsModule,
     FlashMessagesModule.forRoot(),
    RouterModule.forRoot(
     appRoutes,
     { enableTracing: true } // <-- debugging purposes only
   )

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
