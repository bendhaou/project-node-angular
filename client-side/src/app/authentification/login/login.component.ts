import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email:String;
password:String;


  constructor( public router:Router,private _flashMessagesService: FlashMessagesService,public auth:AuthService) { }

  ngOnInit() {
  }
login()
{


  console.log("clicked")
const u={
  email: this.email,
username: "",
  password: this.password


}
  this.auth.login(u).subscribe(data => {
if(data===null)
this._flashMessagesService.show('Email and/or password is invalid', { cssClass: 'alert-danger', timeout: 5000 });


else

this.router.navigate(['home'])
  });

}
}
