import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router } from '@angular/router';
@Component({
  selector: 'app-newpassword',
  templateUrl: './newpassword.component.html',
  styleUrls: ['./newpassword.component.css']
})
export class NewpasswordComponent implements OnInit {
token:String;
confirmpassword:String;
password:String;

  constructor(public router:Router,private route: ActivatedRoute, private auth:AuthService,private _flashMessagesService: FlashMessagesService) {

    this.route.params.subscribe(params => {
  this.token=params["token"];

         });



  }


newpassword(){

  const user={
  password: this.password,

  token: this.token

  }

if(this.password  !== this.confirmpassword)
this._flashMessagesService.show('Password does not match', { cssClass: 'alert-danger', timeout: 5000 });

else


this.auth.newpassword(user).subscribe(data =>
{
 if(data === null ) {
 this._flashMessagesService.show('Password reset token is invalid or has expired, please try again', { cssClass: 'alert-danger', timeout: 7000 });


}



}


);


}
  ngOnInit() {





  }

}
