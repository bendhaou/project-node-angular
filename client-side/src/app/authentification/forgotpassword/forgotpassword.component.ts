import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
email:String
  constructor(public auth:AuthService,private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
  }

reset(){

const user={
  email : this.email
}
this.auth.reset(user).subscribe(data => {
if(data===null)
this._flashMessagesService.show('Email is not found', { cssClass: 'alert-danger', timeout: 5000  })
else

this._flashMessagesService.show('request successfully sended , please check your email ', { cssClass: 'alert-success', timeout: 5000  })
});


}}
