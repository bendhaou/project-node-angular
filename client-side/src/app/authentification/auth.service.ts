import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }


  addUser (newUser :User){

  return this.http.post('http://localhost:3000/users/register',newUser);
}

login (newUser:User ){

  return this.http.post('http://localhost:3000/users/login',newUser);

}

reset (user){

  return this.http.post('http://localhost:3000/users/reset',user);

}
newpassword(user){

   return this.http.post('http://localhost:3000/users/newpassword/'+user.token,user);

}

}
