import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../user';
import {Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private _flashMessagesService: FlashMessagesService,public auth:AuthService, public router:Router) { }
users:any;
message:String;
username:String;
password:String;
email:String;
done:boolean;
  ngOnInit() {
    this.done=false;
  }


clicker(){

  const user={
  username: this.username,
  email:this.email,
  password: this.password

  }
  this.auth.addUser(user).subscribe(data => {

if(data=== null){

this.router.navigate(['home'])


}

else

this._flashMessagesService.show('Account is already exist', { cssClass: 'alert-danger', timeout: 5000  });

  }

);


}

}
