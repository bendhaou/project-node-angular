const express = require('express')
const app = express()
var mongoose = require('mongoose');
var bodyParser = require('body-parser')
var cors = require ('cors')
var users = require('./routers/users');

app.use(cors())
app.use(bodyParser.json())




app.use('/users', users);


app.listen(3000, function () {
  console.log('start server')
})
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
