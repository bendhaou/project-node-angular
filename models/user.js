var mongoose = require('mongoose');
var crypto = require('crypto');
//var uniqueValidator = require('mongoose-unique-validator');


var UserSchema = new mongoose.Schema({
  username: String,
  password:String,
  email:String,
  resetPasswordToken:String,
  resetPasswordExpires:Date,
  isVerified: { type: Boolean, default: false }

  //{type: String, unique: true ,index: true,required: true},
  // bio: String,
  // avatar: String,
  // tel: String,
  // date_naissance: Date,
  // hash: String,
  // salt: String
});

//  UserSchema.plugin(uniqueValidator, {message: 'is already taken.'});
//
// UserSchema.methods.setPassword = function(password){
//    this.salt = crypto.randomBytes(16).toString('hex');
//    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
// };


var User= mongoose.model('User',UserSchema);

module.exports=User;
